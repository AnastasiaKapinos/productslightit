//
//  BaseViewController.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {
    
    lazy private var activityIndicatorView: NVActivityIndicatorView = {
        let activityIndicatorView = NVActivityIndicatorView(frame: self.view.frame, type: .ballPulse, color: Color.purple.value, padding: self.view.frame.width/2.5)
        activityIndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(activityIndicatorView)
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        debugPrint("[deinit]", classForCoder)
    }
    
    func indicatorAnimation(_ start: Bool) {
        start ? activityIndicatorView.startAnimating() : activityIndicatorView.stopAnimating()
    }
    
    func showFailureAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
