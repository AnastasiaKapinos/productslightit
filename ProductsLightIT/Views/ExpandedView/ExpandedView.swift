//
//  ExpandedView.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit
import Cosmos

protocol ExpandedViewToggleDelegate: class {
    func didToggleButtonPressed(_ isExpanded: Bool)
}

protocol ExpandedViewSubmitReviewDelegate: class {
    func didSubmitReviewButtonPressed(review: ReviewForSubmit?)
}

@IBDesignable
class ExpandedView: XibView {
    @IBOutlet weak var toggleButton: UIButton!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var addYourReviewButton: UIButton!
    
    @IBInspectable
    var isExpanded: Bool = false {
        didSet{
            let image = (isExpanded ? expandedImage : unexpandedImage)?.withRenderingMode(.alwaysTemplate)
            toggleButton.setImage(image, for: .normal)
            toggleButton.tintColor = .blue
            
            reviewTextView.isHidden = !isExpanded
            rateView.isHidden = !isExpanded
            submitButton.isHidden = !isExpanded
        }
    }
    
    weak var toggleDelegate: ExpandedViewToggleDelegate?
    weak var submitDelegate: ExpandedViewSubmitReviewDelegate?
    
    private var expandedImage: UIImage?
    private var unexpandedImage: UIImage?
    private var rate = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        guard let expandedImg = UIImage(named: "toggle_up"),
            let unexpandedImg = UIImage(named: "toggle_down") else {
                return
        }
        
        rateView.rating = self.rate
        
        expandedImage = expandedImg
        unexpandedImage = unexpandedImg
        
        isExpanded = false
        
        reviewTextView.delegate = self
        reviewTextView.text = Literals.Review.placeholder
        reviewTextView.textColor = Color.grayPurple.value
        reviewTextView.layer.borderColor = Color.darkPurple.value.cgColor
        reviewTextView.layer.borderWidth = 0.8
        reviewTextView.layer.cornerRadius = 5.0
        
        rateView.didTouchCosmos = { rate in
            self.rate = rate
        }
    }
    
    func clearReviewData() {
        reviewTextView.text = Literals.Review.placeholder
        reviewTextView.textColor = Color.grayPurple.value
        
        self.rate = 0.0
        rateView.rating = self.rate
    }
}

// MARK: - Actions
extension ExpandedView {
    @IBAction func toggleButtonPressed(_ sender: UIButton) {
        isExpanded = !isExpanded
        toggleDelegate?.didToggleButtonPressed(isExpanded)
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        self.handleSubmitReview()
    }
}


// MARK: - UITextViewDelegate
extension ExpandedView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == Color.grayPurple.value {
            textView.text = nil
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = Literals.Review.placeholder
            textView.textColor = Color.grayPurple.value
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            self.handleSubmitReview()
            return false
        }
        return true
    }
}


// MARK: - Private
private extension ExpandedView {
    func fetchReviewFromUI() -> ReviewForSubmit? {
        guard let text = reviewTextView.text,
            text != Literals.Review.placeholder, self.rate != 0 else {
            return nil
        }
        
        return ReviewForSubmit(rate: Int(self.rate), text: text)
    }
    
    func handleSubmitReview() {
        submitDelegate?.didSubmitReviewButtonPressed(review: fetchReviewFromUI())
    }
}

