//
//  ProductReviewTableViewCell.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/21/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit
import Cosmos

class ProductReviewTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var infoUserDateLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var commentsLabel: UILabel!
    
    // MARK: - Properties
    private var review: Review?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateView.isUserInteractionEnabled = false
        self.selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        rateView.prepareForReuse()
        infoUserDateLabel.text = ""
        commentsLabel.text = ""
    }
}


// MARK: - Public
extension ProductReviewTableViewCell {
    func configure(by review: Review) {
        self.review = review
        
        self.infoUserDateLabel.attributedText = self.formattedCreationalInfo()
        self.rateView.rating = Double(review.rate ?? 1)
        self.commentsLabel.text = review.text ?? ""
    }
}


// MARK: - Private
private extension ProductReviewTableViewCell {
    func formattedCreationalInfo() -> NSMutableAttributedString {
        var dateString = ""
        if let createdAt = self.review?.createdAt {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a 'on' MMMM dd, yyyy"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            
            dateString = " at " + formatter.string(from: createdAt)
        }
        
        let user = self.review?.createdBy?.username ?? ""
        let userAttribute = [NSAttributedString.Key.font: UIFont(name: "AvenirNextCondensed-MediumItalic", size: 17.0)!]
        let dateAttribute = [NSAttributedString.Key.font: UIFont(name: "AvenirNextCondensed-Italic", size: 15.0)!]
        
        let result = NSMutableAttributedString()
        result.append(NSAttributedString(string: user,       attributes: userAttribute))
        result.append(NSAttributedString(string: dateString, attributes: dateAttribute))
        
        return result
    }
}
