//
//  ProductReviewTableViewDataSource.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/22/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class ProductReviewTableViewDataSource: NSObject, UITableViewDataSource {
    var reviews: Reviews?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Literals.Cells.review, for: indexPath) as? ProductReviewTableViewCell else {
            return ProductReviewTableViewCell()
        }
        
        if let reviews = reviews, reviews.indices.contains(indexPath.row) {
            cell.configure(by: reviews[indexPath.row])
        }
        return cell
    }
}


