//
//  CarouselCollectionDataSource.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/21/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class CarouselCollectionDataSource: NSObject, UICollectionViewDataSource {
    var products: Products?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Literals.Cells.product, for: indexPath) as? CarouselCollectionViewCell else {
            return CarouselCollectionViewCell()
        }
        
        if let products = products, products.indices.contains(indexPath.row) {
            cell.configure(with: products[indexPath.row])
        }
        
        return cell
    }
}
