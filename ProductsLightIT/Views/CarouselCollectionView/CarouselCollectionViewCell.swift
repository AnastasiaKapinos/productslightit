//
//  CarouselCollectionViewCell.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/21/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class CarouselCollectionViewCell: UICollectionViewCell {
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = nil
        imageView.image = UIImage(named: "product_placeholder")
    }
    
    // MARK: - Public
    func configure(with product: Product) {
        titleLabel.text = product.title
        setImage(by: product.imageName)
        
        highlight(false)
    }
    
    func highlight(_ isHighlight: Bool) {
        contentView.layer.borderColor = isHighlight ? Color.purple.value.cgColor: Color.lightGray.value.cgColor
        contentView.layer.borderWidth = isHighlight ? 1.5 : 1
    }
}

// MARK: - Private
private extension CarouselCollectionViewCell {
    func setImage(by imageName: String) {
        NetworkManagerImpl.shared.getImage(by: imageName) { [weak self] result in
            switch result {
            case .success(let image):
                self?.imageView.image = image
            case .failure(let error):
                print("Error on image downloading: ", error.localizedDescription)
            }
        }
    }
}

