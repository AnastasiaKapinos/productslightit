//
//  Product.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

typealias Products = [Product]
struct Product: Codable, CustomStringConvertible {
    let id:         Int
    let title:      String
    let imageName:  String
    let descr:      String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case imageName = "img"
        case descr     = "text"
    }
    
    var description: String {
        let str = """
        \nid:  \(id)
        title: \(title)
        img:   \(imageName)
        descr: \(descr)
        """
        return str
    }
}

struct ProductFull {
    var id:     Int
    var title:  String?
    var image:  UIImage?
    var descr:  String?
    
    init(by product: Product, image: UIImage?) {
        self.id     = product.id
        self.title  = product.title
        self.descr  = product.descr
        self.image  = image
    }
}
