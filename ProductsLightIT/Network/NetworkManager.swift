//
//  NetworkManager.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

protocol NetworkManager {
    func getProducts(completion: @escaping(Result<Products, Error>) -> ())
    func getReviews(by productID: Int, completion: @escaping(Result<Reviews, Error>) -> ())
    func getImage(by imageName: String, completion: @escaping(Result<UIImage, Error>) -> ())
    
    func login(username: String, password: String, completion: @escaping(Result<Login, Error>) -> ())
    func register(username: String, password: String, completion: @escaping(Result<Login, Error>) -> ())
    
    func submitReview(review: ReviewForSubmit, id: Int, completion: @escaping(Result<SubmittedReview, Error>) -> ())
}

class NetworkManagerImpl {
    static let shared = NetworkManagerImpl()
    
    let kEndpointURL = "http://smktesting.herokuapp.com/"
    
    enum ApiRoutes: String {
        case register = "api/register/"
        case login    = "api/login/"
        
        case productsList    = "api/products/"
        case productReviews  = "api/reviews/"
        case productImage    = "static/"
    }
    
    private let parseError = NSError(domain: "productsLightIT", code: 1000, userInfo: [NSLocalizedDescriptionKey : "Unable to parse data"])
    
    private init() { }
}

//MARK: - Private
private extension NetworkManagerImpl {
    func url(_ endpoint: ApiRoutes, additionalPath: String = "") -> URL {
        let fullEndpoint = kEndpointURL + endpoint.rawValue + additionalPath
        guard let url = URL(string: fullEndpoint) else { fatalError("no url") }
        return url
    }
    
    func header() -> [String: String] {
        guard let token = UserManager.shared.token else { return [:] }
        return ["Authorization": "Token " + token]
    }
}

extension NetworkManagerImpl: NetworkManager {
    func getImage(by imageName: String, completion: @escaping (Result<UIImage, Error>) -> ()) {
        Alamofire.request(self.url(.productImage, additionalPath: imageName)).responseImage { response in
            switch response.result {
            case .success(_):
                if let image = response.result.value {
                    completion(.success(image))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getProducts(completion: @escaping(Result<Products, Error>) -> ()) {
        Alamofire.request(self.url(.productsList), method: .get, parameters: nil).responseJSON { response in
            switch response.result {
            case .success(_):
                if let data = response.data, let result = try? JSONDecoder().decode(Products.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getReviews(by productID: Int, completion: @escaping(Result<Reviews, Error>) -> ()) {
        Alamofire.request(self.url(.productReviews, additionalPath: "\(productID)"), method: .get, parameters: nil).responseJSON { response in
            switch response.result {
            case .success(_):
                if let data = response.data, let result = try? JSONDecoder().decode(Reviews.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func login(username: String, password: String, completion: @escaping(Result<Login, Error>) -> ()) {
        let params = ["username": username,
                      "password": password]
        
        Alamofire.request(self.url(.login), method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(_):
                if let data = response.data, let result = try? JSONDecoder().decode(Login.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func register(username: String, password: String, completion: @escaping(Result<Login, Error>) -> ()) {
        let params = ["username": username,
                      "password": password]
        
        Alamofire.request(self.url(.register), method: .post, parameters: params).responseJSON { response in
            switch response.result {
            case .success(_):
                if let data = response.data, let result = try? JSONDecoder().decode(Login.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func submitReview(review: ReviewForSubmit, id: Int, completion: @escaping(Result<SubmittedReview, Error>) -> ()) {
        let params = ["rate": "\(review.rate)",
                      "text": review.text]
        
        Alamofire.request(self.url(.productReviews, additionalPath: "\(id)"), method: .post, parameters: params, encoding: JSONEncoding.default, headers: header()).responseJSON { response in
            switch response.result {
            case .success(_):
                if let data = response.data, let result = try? JSONDecoder().decode(SubmittedReview.self, from: data) {
                    completion(.success(result))
                } else {
                    completion(.failure(self.parseError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
