//
//  User.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: Int
    let username:   String?
    let firstName:  String?
    let lastName:   String?
    let email:      String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case username
        case firstName = "first_name"
        case lastName  = "last_name"
        case email
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id        = try container.decode(Int.self, forKey: .id)
        username  = try container.decodeIfPresent(String.self, forKey: .username)
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        lastName  = try container.decodeIfPresent(String.self, forKey: .lastName)
        email     = try container.decodeIfPresent(String.self, forKey: .email)
    }
    
    init() {
        id          = -1
        username    = ""
        firstName   = ""
        lastName    = ""
        email       = ""
    }
    
    var description: String {
        let str = """
        \nid:       \(id)
        username:   \(username  ?? "")
        firstName:  \(firstName ?? "")
        lastName:   \(lastName  ?? "")
        email:      \(email     ?? "")
        """
        return str
    }
}
