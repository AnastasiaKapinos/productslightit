//
//  Review.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

typealias Reviews = [Review]

struct Review: Codable, CustomStringConvertible {
    let id:         Int
    let productID:  Int?
    let rate:       Int?
    let text:       String?
    let createdBy:  User?
    let createdAt:  Date?
    
    enum CodingKeys: String, CodingKey {
        case id
        case productID = "product"
        case rate
        case text
        case createdBy = "created_by"
        case createdAt = "created_at"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id            = try container.decode(Int.self, forKey: .id)
        productID     = try container.decodeIfPresent(Int.self, forKey: .productID)
        rate          = try container.decodeIfPresent(Int.self, forKey: .rate)
        text          = try container.decodeIfPresent(String.self, forKey: .text)
        createdBy     = try container.decodeIfPresent(User.self, forKey: .createdBy)
        
        guard let date = try container.decodeIfPresent(String.self, forKey: .createdAt) else {
            createdAt = Date()
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        createdAt = formatter.date(from: date)
    }
    
    var description: String {
        let str = """
        \nid:       \(id)
        product:    \(productID ?? -1)
        rate:       \(rate ?? -1)
        text:       \(text ?? "")
        createdBy:  \(createdBy?.description ?? "no user")
        crearedAt:  \(createdAt ?? Date())
        """
        return str
    }
}

struct ReviewForSubmit {
    var rate: Int
    var text: String
}


struct SubmittedReview: Codable {
    let success: Bool
}
