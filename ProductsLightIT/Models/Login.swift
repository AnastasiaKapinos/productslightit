//
//  Login.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

struct Login: Codable {
    let success: Bool
    let token:   String?
    let message: String?
}
