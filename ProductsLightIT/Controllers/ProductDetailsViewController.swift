//
//  ProductDetailsViewController.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/20/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class ProductDetailsViewController: BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var reviewExpandedView: ExpandedView!
    @IBOutlet weak var reviewsTableView: UITableView!
    
    // MARK: - Properties
    var product: ProductFull?
    private let tableViewDataSource = ProductReviewTableViewDataSource()
    private var pendingReview: ReviewForSubmit?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureVC()
    }
    
    @IBAction func unwindSegueToMasterVC(_ sender: UIStoryboardSegue) {
        if let source = sender.source as? RegistrationViewController {
            self.submitReviewForProduct()
        }
    }
}

// MARK: - Private
private extension ProductDetailsViewController {
    func configureVC() {
        guard let product = self.product else {
            return
        }
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.navigationItem.title = product.title
        self.descriptionLabel.text = product.descr
        self.imageView.image = product.image
        
        reviewsTableView.delegate = self
        reviewsTableView.dataSource = tableViewDataSource
        
        reviewExpandedView.toggleDelegate = self
        reviewExpandedView.submitDelegate = self
        
        self.fetchReviewsForProduct(by: product.id)
    }
    
    func fetchReviewsForProduct(by id: Int) {
        self.indicatorAnimation(true)
        
        NetworkManagerImpl.shared.getReviews(by: id) { [weak self] result in
            self?.indicatorAnimation(false)
            switch result {
            case .success(let reviews):
                self?.tableViewDataSource.reviews = reviews
                self?.reviewsTableView.reloadData()
            case .failure(let error):
                self?.showFailureAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
    
    func submitReviewForProduct() {
        guard let id = self.product?.id, let review = pendingReview else { return }
        
        self.indicatorAnimation(true)
        NetworkManagerImpl.shared.submitReview(review: review, id: id) { [weak self] result in
            self?.indicatorAnimation(false)
            switch result {
            case .success(let submitted):
                if submitted.success {
                    self?.fetchReviewsForProduct(by: id)
                    self?.reviewExpandedView.isExpanded = false
                    self?.reviewExpandedView.clearReviewData()
                    self?.view.showToast(toastMessage: Literals.Review.successSubmit, duration: 1)
                } else {
                    self?.showFailureAlert(title: "Error", message: Literals.Review.errorSubmit)
                }
            case .failure(let error):
                self?.showFailureAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
}

extension ProductDetailsViewController: ExpandedViewToggleDelegate {
    func didToggleButtonPressed(_ isExpanded: Bool) {
        var frameNew = self.reviewExpandedView.frame
        frameNew.size.height = isExpanded ? 260 : 50
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
            self?.reviewExpandedView.frame = frameNew
        }, completion: nil)
    }
}

extension ProductDetailsViewController: ExpandedViewSubmitReviewDelegate {
    func didSubmitReviewButtonPressed(review: ReviewForSubmit?) {
        guard let review = review else {
            self.view.showToast(toastMessage: Literals.Review.errorNoReview, duration: 1.5)
            return
        }
        
        self.pendingReview = review
        if UserManager.shared.token != nil {
            self.submitReviewForProduct()
        } else {
            let alert = UIAlertController(title: Literals.Review.sendReview, message: Literals.Review.errorNoRegister, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.performSegue(withIdentifier: Literals.Segue.login, sender: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDelegate
extension ProductDetailsViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.reviewExpandedView.isExpanded = false
    }
}


// MARK: - Navigation
extension ProductDetailsViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == Literals.Segue.login,
            let vc = segue.destination as? LoginViewController {
            vc.completionHandler = { [weak self] loggedInMsg in
                self?.submitReviewForProduct()
            }
        }
    }
}
