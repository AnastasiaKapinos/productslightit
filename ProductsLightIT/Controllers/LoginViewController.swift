//
//  LoginViewController.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    var completionHandler:((String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureViewController()
    }
}

// MARK: - Private
private extension LoginViewController {
    func configureViewController() {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        loginButton.layer.cornerRadius = 5.0
        
        usernameTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    func validateDataAndLogin() {
        guard let username = usernameTextField.text, !username.isEmpty else {
            self.view.showToast(toastMessage: Literals.Login.errorNoUsername, duration: 1.5)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            self.view.showToast(toastMessage: Literals.Login.errorNoPassword, duration: 1.5)
            return
        }
        
        self.indicatorAnimation(true)
        
        NetworkManagerImpl.shared.login(username: username, password: password) { [weak self] result in
            self?.indicatorAnimation(false)
            switch result {
            case .success(let login):
                if login.success, let token = login.token {
                    UserManager.shared.token = token
                    _ = self?.completionHandler?("Logged in")
                    self?.navigationController?.popViewController(animated: false)
                } else {
                    self?.showFailureAlert(title: "Error", message: login.message ?? Literals.Login.errorLogin)
                }
            case .failure(let error):
                self?.showFailureAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
}

// MARK: - User Interactions
private extension LoginViewController {
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        self.validateDataAndLogin()
    }
}


// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            passwordTextField.becomeFirstResponder()
        case passwordTextField:
            passwordTextField.resignFirstResponder()
            self.validateDataAndLogin()
        default: break
        }
        
        return true
    }
}

