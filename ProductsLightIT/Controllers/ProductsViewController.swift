//
//  ProductsViewController.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class ProductsViewController: BaseViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    private let layout = CarouselCollectionFlowLayout()
    private let collectionDataSource = CarouselCollectionDataSource()
    private var products: Products?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = ""
        
        self.indicatorAnimation(true)
        
        NetworkManagerImpl.shared.getProducts { [weak self] result in
            self?.indicatorAnimation(false)
            switch result {
            case .success(let products):
                self?.products = products
                self?.configureCollectionView()
            case .failure(let error):
                self?.showFailureAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
}


// MARK: - Private
private extension ProductsViewController {
    func configureCollectionView() {
        collectionView.delegate = self
        
        collectionDataSource.products = self.products
        collectionView.dataSource = collectionDataSource
        
        layout.itemSize = CGSize(width: 150, height: 200)
        layout.zoomFactor = 0.4
        collectionView.collectionViewLayout = layout
    }
}


// MARK: - UICollectionViewDelegate
extension ProductsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let products = self.products, products.indices.contains(indexPath.row),
            let selectedCell = collectionView.cellForItem(at: indexPath) as? CarouselCollectionViewCell else {
            return
        }
        
        for case let carouselCell as CarouselCollectionViewCell in collectionView.visibleCells {
            carouselCell.highlight(false)
        }
        selectedCell.highlight(true)
        
        performSegue(withIdentifier: Literals.Segue.productDetails,
                     sender: ProductFull(by: products[indexPath.row], image: selectedCell.imageView.image))
    }
}


// MARK: - Navigation
extension ProductsViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        if identifier == Literals.Segue.productDetails,
            let vc = segue.destination as? ProductDetailsViewController,
            let product = sender as? ProductFull {
            vc.product = product
        }
    }
}
