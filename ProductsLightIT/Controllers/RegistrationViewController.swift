//
//  RegistrationViewController.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

class RegistrationViewController: BaseViewController {

    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViewController()
    }
}


// MARK: - User Interactions
private extension RegistrationViewController {
    @IBAction func registrationButtonPressed(_ sender: UIButton) {
        self.validateDataAndRegister()
    }
}

// MARK: - Private
private extension RegistrationViewController {
    func configureViewController() {
        self.navigationController?.navigationBar.topItem?.title = ""
        
        registrationButton.layer.cornerRadius = 5.0
        
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        passwordConfirmationTextField.delegate = self
    }
    
    func validateDataAndRegister() {
        guard let password = passwordTextField.text, !password.isEmpty else {
            self.view.showToast(toastMessage: Literals.Register.errorNoPassword, duration: 1.5)
            return
        }
        
        guard let passwordConfirm = passwordConfirmationTextField.text, !passwordConfirm.isEmpty else {
            self.view.showToast(toastMessage: Literals.Register.errorNoConfirm, duration: 1.5)
            return
        }
        
        guard password == passwordConfirm else {
            self.view.showToast(toastMessage: Literals.Register.errorNotEquals, duration: 1.5)
            return
        }
        
        guard let username = usernameTextField.text, !username.isEmpty else {
            self.view.showToast(toastMessage: Literals.Register.errorNoUsername, duration: 1.5)
            return
        }
        
        self.indicatorAnimation(true)
        
        NetworkManagerImpl.shared.register(username: username, password: password) { [weak self] result in
            self?.indicatorAnimation(false)
            switch result {
            case .success(let register):
                if register.success, let token = register.token {
                    UserManager.shared.token = token
                    self?.performSegue(withIdentifier: Literals.Segue.unwindToDetails, sender: nil)
                } else {
                    self?.showFailureAlert(title: "Error", message: register.message ?? Literals.Register.errorRegister)
                }
            case .failure(let error):
                self?.showFailureAlert(title: "Error", message: error.localizedDescription)
            }
        }
    }
}

// MARK: - UITextFieldDelegate
extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case passwordTextField:
            passwordConfirmationTextField.becomeFirstResponder()
        case passwordConfirmationTextField:
            usernameTextField.becomeFirstResponder()
        case usernameTextField:
            usernameTextField.resignFirstResponder()
            self.validateDataAndRegister()
        default: break
        }
        
        return true
    }
}
