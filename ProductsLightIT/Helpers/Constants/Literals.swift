//
//  Literals.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

enum Literals {
    enum Review {
        static let placeholder     = "Your review"
        static let sendReview      = "Send review"
        static let errorNoReview   = "Please provide the review for this product and it's rate. \nThese data couldn't be empty"
        static let errorNoRegister = "Only registred users can send reviews"
        static let errorSubmit     = "Error on sumbit review"
        static let successSubmit   = "Your review has been added"
    }
   
    enum Segue {
        static let productDetails = "productDetailsSegue"
        static let login          = "loginSegue"
        static let register       = "registerSegue"
        static let unwindToDetails = "unwindSegueToProductDetails"
    }
    
    enum Login {
        static let errorNoUsername = "Input username"
        static let errorNoPassword = "Input password"
        static let errorLogin      = "Error on login"
    }
    
    enum Register {
        static let errorNoUsername = "Input username"
        static let errorNoPassword = "Input password"
        static let errorNoConfirm  = "Input password confirmation"
        static let errorNotEquals  = "Password and it's confirmation should be equals"
        static let errorRegister   = "Error on register"
    }
    
    enum Cells {
        static let product = "carouselCell"
        static let review  = "reviewCell"
    }
}
