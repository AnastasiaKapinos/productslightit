//
//  Color.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/20/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

enum Color {
    case lightGray
    case lightBlue
    case purple
    case grayPurple
    case darkPurple
}

extension Color {
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
        case .lightGray:    instanceColor = UIColor(red: 215/255.0, green: 216/255.0, blue: 217/255.0, alpha: 1)
        case .lightBlue:    instanceColor = UIColor(red: 118/255.0, green: 170/255.0, blue: 251/255.0, alpha: 1)
        case .purple:       instanceColor = UIColor(red: 87/255.0,  green: 51/255.0,  blue: 131/255.0, alpha: 1)
        case .grayPurple:   instanceColor = UIColor(red: 87/255.0,  green: 80/255.0,  blue: 131/255.0, alpha: 1)
        case .darkPurple:   instanceColor = UIColor(red: 1/255.0,   green: 25/255.0,  blue: 147/255.0, alpha: 1)
        }
        return instanceColor
    }
}
