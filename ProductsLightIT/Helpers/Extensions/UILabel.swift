//
//  UILabel.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//
import UIKit

extension UILabel {
    func setFullName(firstName: String, lastName: String) {
        let firstAttribute = [NSAttributedString.Key.foregroundColor: UIColor.red,
                              NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.font?.pointSize ?? 10, weight: .bold)]
        let firstString = NSMutableAttributedString(string: firstName, attributes: firstAttribute)
        let lastAttribute = [NSAttributedString.Key.foregroundColor: UIColor.red,
                             NSAttributedString.Key.font: UIFont.systemFont(ofSize: self.font?.pointSize ?? 10, weight: .regular)]
        let lastString = NSMutableAttributedString(string: " \(lastName)", attributes: lastAttribute)
        
        firstString.append(lastString)
        
        self.attributedText = firstString
    }
}

// MARK: - adding insets
extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: rect.inset(by: insets))
        } else {
            self.drawText(in: rect)
        }
    }
}

