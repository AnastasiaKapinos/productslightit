//
//  CGFloat.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import UIKit

extension CGFloat {
    func getminimum(value2:CGFloat)->CGFloat {
        if self < value2 {
            return self
        }
        else {
            return value2
        }
    }
}
