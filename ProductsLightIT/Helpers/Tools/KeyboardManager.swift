//
//  KeyboardManager.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/24/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

class KeyboardManager {
    
    static func setup() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    static func enableIQKeyboard(_ enable: Bool) {
        IQKeyboardManager.shared.enable = enable
    }
}
