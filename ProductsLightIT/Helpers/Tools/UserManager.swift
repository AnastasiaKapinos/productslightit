//
//  UserManager.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

class UserManager {
    
    private static let tokenValue = "token"
    
    static let shared = UserManager()
    
    var token: String? {
        get { return UserDefaults.standard.string(forKey: UserManager.tokenValue) }
        set { UserDefaults.standard.set(newValue, forKey: UserManager.tokenValue) }
    }
}
