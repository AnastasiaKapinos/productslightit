//
//  Result.swift
//  ProductsLightIT
//
//  Created by Anastasia Kapinos on 6/19/19.
//  Copyright © 2019 Anastasia Kapinos. All rights reserved.
//

import Foundation

enum Result<T, Error: Swift.Error> {
    case success(T)
    case failure(Error)
}
